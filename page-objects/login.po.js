const Helper = require('protractor-helper')

class Login {

    doLogin(username, password){
        const inputEmail = element(by.css("input[name='username']"));
        const inputPassword = element(by.css("input[name='password']"));
        const btnSignIn = element(by.css('.button-sign-in'));
        Helper.fillFieldWithText(inputEmail, username)
        Helper.fillFieldWithText(inputPassword, password)
        Helper.click(btnSignIn)
    }
}

module.exports = Login;