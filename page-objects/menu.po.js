const Helper = require('protractor-helper')

class Menu {

    addItemToCart(value) {
        const itemPickUp = element(by.cssContainingText('item.ng-scope.ng-isolate-scope', value))
        Helper.click(itemPickUp)
    }

    clickCollectionOption() {
        const radioButtonOption = element(by.cssContainingText('.radio-label.ng-binding', 'Collection'));
        browser.sleep(1000)
        Helper.click(radioButtonOption)
    }

    clickDeliveryOption() {
        const radioButtonOption = element(by.cssContainingText('.radio-label.ng-binding', 'Delivery'));
        browser.sleep(1000)
        Helper.click(radioButtonOption)
    }

    clickButtonDeliveryAdress() {
        const btnDelivery = element(by.buttonText('Choose delivery address'))
        Helper.click(btnDelivery)
    }

    setAdressOnDeliveryOption() {
        const inputAdress = element(by.css('input[name=autoCompleteAddress]'))
        const clickAdressOptin = element(by.tagName('ul.dropdown-menu'))
        Helper.fillFieldWithText(inputAdress, '68 Radcliffe Road Southampton SO140PN')
        Helper.click(clickAdressOptin)
    }

    clickButtonContinue() {
        const btnContinue = element(by.buttonText('Continue'))
        browser.sleep(1000)
        Helper.click(btnContinue)
    }

    setDay() {
        const dayDropdown = element.all(by.tagName('div.dropdown')).get(0)
        const dayDropdownOption = element.all(by.repeater('option in collection')).get(0)
        Helper.click(dayDropdown)
        Helper.click(dayDropdownOption)
    }

    setTime() {
        const timeDropdown = element.all(by.tagName('div.dropdown')).get(1)
        const timeDropdownOption = element(by.cssContainingText("div[name='time'] li[ng-repeat='option in collection']", 'ASAP'))
        Helper.click(timeDropdown)
        Helper.click(timeDropdownOption)
    }

    clickButtonDone() {
        const btnDone = element(by.buttonText('Done'))
        Helper.click(btnDone)
    }

}

module.exports = Menu;