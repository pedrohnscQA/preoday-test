const Helper = require('protractor-helper')

class Checkout{

    clickCheckout(){
        const btnCheckout = element(by.buttonText('Checkout'))
        Helper.click(btnCheckout)
    }

    placeOrder(){
        const btnPlaceOrder = element(by.buttonText('Place My Order'));
        browser.sleep(4000)
        Helper.click(btnPlaceOrder,12000)
    }

    getOrderDetais(){
        const orderDetails = element(by.css('.order-details.ng-scope'));
        browser.sleep(4000)
        return orderDetails.getText()
    }

    selectPaymentMethod(){
        const paymentMethod = element(by.cssContainingText('.radio-label.ng-binding', 'Card'));
        browser.sleep(4000)
        Helper.click(paymentMethod)
    }
}

module.exports = Checkout;