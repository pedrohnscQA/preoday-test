const Helper = require('protractor-helper')
class Contact{

    clickOnContactTab(){
        const contactTab = element(by.css('#tab-contact'))
        browser.sleep(5000)
        Helper.click(contactTab)
    }

    getContactInformation(){
        const contactInformation = element(by.tagName('md-card-content'))
        return contactInformation.getText()
    }
}
module.exports = Contact;