
module.exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    baseUrl: "https://preoday:PreoDay123@menus-dev.preoday.com/qatraining",
    specs: ["specs/*.spec.js"],
    capabilities: {
        'browserName': 'chrome',
    },
    restartBrowserBetweenTests: true,

    onPrepare: () => {
        browser.ignoreSynchronization = false;
    }
}
