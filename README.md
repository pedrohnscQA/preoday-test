# Preoday Tests 

Web Order Test

## How to execute tests

**1** - Install Node.js: https://nodejs.org/en/download/

**2** - Make a clone from this repositpry

**3** - 
Open CMD until it is inside the 'preoday-test' directory and type the following command to install the dependencies:
```
  npm install
```
  
**4** - 
 To run the test type the command:
```
  npm test
```

##Considerations##
In this test I covered 5 test cases described in this document, including checking the contact screen, making a purchase using collection and delivery, both using cash and card payment methods.

I had some initial issues with element mapping, but that was resolved after some trying and help.

I used the protractor-helper library to prevent the tests from becoming flakiness, but in some cases it was not possible to make full use of it, so it was necessary to leave some explicit waits.

I mostly chose to automate testing where payment is required because it is a more critical feature, and the contact part, in case the user wants to contact preoday
