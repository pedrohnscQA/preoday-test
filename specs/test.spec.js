const Login = require('../page-objects/login.po')
const Menu = require('../page-objects/menu.po')
const Checkout = require('../page-objects/checkout.po')
const Contact = require('../page-objects/contact.po')

describe('Purchase something', () => {
    const loginPage = new Login
    const menuPage = new Menu
    const checkoutPage = new Checkout
    const contactPage = new Contact

    beforeEach(() => {
        browser.get(browser.baseUrl)
    })

    afterEach(() => {
        browser.restartSync()
    })

    it('Access the contact tab', () => {
        contactPage.clickOnContactTab()
        expect(contactPage.getContactInformation()).toContain('Address')
    })
    
    it('Successfull purchase with cash (collection)', () => {
        menuPage.addItemToCart('Rice')
        checkoutPage.clickCheckout()
        menuPage.clickCollectionOption()
        menuPage.setDay()
        menuPage.setTime()
        menuPage.clickButtonDone()
        loginPage.doLogin('pedrohnsc@gmail.com', '1550220pH@')
        checkoutPage.placeOrder()
        expect(checkoutPage.getOrderDetais()).toContain('TOTAL')
    })

    it('Successfull purchase with Card', () => {
        browser.waitForAngularEnabled(false)
        menuPage.addItemToCart('Rice')
        checkoutPage.clickCheckout()
        menuPage.clickCollectionOption()
        menuPage.setDay()
        menuPage.setTime()
        menuPage.clickButtonDone()
        loginPage.doLogin('pedrohnsc@gmail.com', '1550220pH@')
        checkoutPage.selectPaymentMethod()
        checkoutPage.placeOrder()
        expect(checkoutPage.getOrderDetais()).toContain('TOTAL')
    })


    it('Place a delivery order paying in cash', ()=>{
        menuPage.addItemToCart('Rice')
        checkoutPage.clickCheckout()
        menuPage.clickDeliveryOption()
        menuPage.clickButtonDeliveryAdress()
        menuPage.setAdressOnDeliveryOption()
        menuPage.clickButtonContinue()
        menuPage.clickButtonContinue()
        loginPage.doLogin('pedrohnsc@gmail.com', '1550220pH@')
        checkoutPage.placeOrder()
        expect(checkoutPage.getOrderDetais()).toContain('TOTAL')
    })

    it('Place a delivery order paying in card', ()=>{
        menuPage.addItemToCart('Rice')
        checkoutPage.clickCheckout()
        menuPage.clickDeliveryOption()
        menuPage.clickButtonDeliveryAdress()
        menuPage.setAdressOnDeliveryOption()
        menuPage.clickButtonContinue()
        menuPage.clickButtonContinue()
        loginPage.doLogin('pedrohnsc@gmail.com', '1550220pH@')
        checkoutPage.selectPaymentMethod()
        checkoutPage.placeOrder()
        expect(checkoutPage.getOrderDetais()).toContain('TOTAL')
    })



})